/** 
 * chris@chriscummings.net
 * http://www.chriscummings.net
 */

// Path Class
Path = function(element){

	// Make new operator optional.
	if(!(this instanceof xena.Path)){
		return new xena.Path(element);
	}

	this.selectors = [];

	var currentElement = element,
		go = true;

	do{
		// Stop if we run into something that will break code like going beyond 
		// the html object.
		if (typeof currentElement.tagName === 'undefined'){
			break;
		}

		var id = currentElement.id;

		// If we find an element with an ID, we can use that as the path root 
		// and stop ascending the DOM now.
        if(id){
			this.selectors.push('#' + id);

			// Comment out this break if you want the path to extend all the 
			// way up the DOM regardless of finding an element with an ID.
			break;

		// Othwerwise, use the element tagName and its sibling index (if it 
		// needs one).
		// Examples: "li" vs. "li:nth-child(9)"
        }else{
			var tagName      = currentElement.tagName,
				pathSegment  = tagName.toLowerCase(),
				elementIndex = this.siblingsIndex(currentElement);

			// If this element has a real index (that is, it has siblings) 
			// append an nth-child string to the pathSegment.
			if(elementIndex >= 0){
				pathSegment += ':nth-child(' + elementIndex + ')';
			}

            this.selectors.push(pathSegment);
        }

        // If there is yet another parent node, set the currentElement to the 
        // parent and continue ascending the DOM.
		if(currentElement.parentNode){
			currentElement = currentElement.parentNode;

		// Otherwise, we're done.
		}else{
			go = false;
		}
	}while(go);
};

// Returns CSS selector path.
// Example: 
// #entry-longdesc > div:nth-child(2) > table > tbody > tr > td:nth-child(1)
Path.prototype.path = function(){
	return this.selectors.reverse().join(' > ');
};

// Returns an element's index amongst its siblings. 
// Returns -1 when no parent exists or no other siblings exist.
xena.Path.prototype.siblingsIndex = function(element){
	var parentNode   = element.parentNode,
		tagName      = element.tagName,
		siblings     = [],
		children;

	if(!parentNode){
		return -1;
	}

	// parentNode.children isn't 100% normalized across browsers:
	// http://www.quirksmode.org/dom/w3c_core.html#t70
	children = parentNode.childNodes;

	// Loop through children and check for elements with the same tag name.
	for(var i=0; i<children.length; i++){
		var node = children[i];
		// Since some browsers will consider text nodes as children, check 
		// that node even has a tagName property.
		if(node.tagName !== 'undefined' && node.tagName === tagName){
			siblings.push(children[i]);
		}
	}

	if(siblings.length === 1){
		// Element has no silbings.
		return -1;
	}

	return siblings.indexOf(element);
};

/*
// Quick testing.
jQuery('html *').on('click', function(e){
    e.stopPropagation();
    console.log(Path(this).path());
});
*/