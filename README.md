Path.js
=======

A cross-browser, vanilla (library independent) JavaScript class to derive the CSS selector path for a given element.

Usage
-----

`var myPath = Path(someListItemElement).path() // #sidebar > ul > li:nth-child(5)`

or

`var myPath = new Path(someListItemElement).path() // #sidebar > ul > li:nth-child(5)`

Misc
----

Apologies for GitHub's terrible tab rendering when viewing source code.